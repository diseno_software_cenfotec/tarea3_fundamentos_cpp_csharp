// Ejercicio 1 Tarea 3 Matías Leandro Flores
#include <iostream>
#include <memory>

//using namespace std;

/**
 * @brief Estructura que almacena datos de una camara, cantidad disparos, marca, año.
*/
struct Camara{
    int Disparos = 0;  // inicia con 0 disparos tomados
    int año;
    char *marca;
};

/**
 * @brief Elimina struct de camara de la mem si sobrepaso límite de disparos.
 * @param camara: referencia a shared ptr de struct de camara con el cual trabajar.
*/
void VerifyShots(std::shared_ptr<Camara> &camara, int limite_disparos){
    if (camara->Disparos >= limite_disparos){
        std::cout << "Alcanzó el límite de disparos" << std::endl;
        camara.reset();  // camara para de referenciar objeto (use_count-1)
    }
}

/**
 * @brief Returna 1 si objeto al que apunta parametro ya fue destruido (tiene 0 referencias)
 * @brief .... y 0 si sigue existiendo.
 * @param shared: Puntero a objeto a revisar.
 * 
*/
bool VerifyDestroyed(std::shared_ptr<Camara> &shared){
    // weak ptr para verificar si tiene referencias activas
    std::weak_ptr<Camara> checker = shared; 
    if (checker.use_count() == 0){
        std::cout<< "Objeto referenciado fue destruido" << std::endl;
        return 1;
    }
    else {
        std::cout << "Objeto existe" << std::endl;
        return 0;
    }
}


int main(){
    int limite_disparos = 6;
    auto camara = std::make_shared<Camara>();
    camara->año = 1995;
    camara->marca = "Sony";

    std::cout << "El límite de disparos es de " << limite_disparos << std::endl;

    // Ciclo que aumenta cantidad de disparos hasta que ya no exista objeto camara
    while (VerifyDestroyed(camara) == false)
    {
        camara->Disparos++;
        std::cout << "Cantidad de disparos: " << camara->Disparos << std::endl;
        VerifyShots(camara, limite_disparos);  // si sobrepasa límite, se destruye camara
    }

    return 0;
}

