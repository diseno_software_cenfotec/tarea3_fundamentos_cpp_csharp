#pragma once

#include <memory>

namespace Recordatorios {


struct Recordatorio {   // o usar libreria string
    int id;
    char* nota;
    char* fecha;
    char* hora;
};

struct Node;

// Un nodo incluye un recordatorio
struct Node {
    std::shared_ptr<Node> prev = nullptr;
    std::shared_ptr<Node> next = nullptr;
    std::shared_ptr<Recordatorio> recordatorio;
};

 class Lista
    {
    public:
        void insertar_dato(std::shared_ptr<Recordatorio> dato);
        void mostrar_dato(int id);
        void eliminar_dato(int id);
        void print_lista();

        /**
         * Constructor
        */
        Lista() {
            this->head = nullptr;
            this->tail = nullptr;
        }

        /**
         * Destructor
        */
        ~Lista() {}

    private:
        std::shared_ptr<Node> head;
        std::shared_ptr<Node> tail;
        int tamanho;
    };

}
