#include <iostream>
#include <memory>
#include <string>

#include "list.hpp"

std::shared_ptr<Recordatorios::Recordatorio> CrearRecordatorio(){
    auto recordatorio = std::make_shared<Recordatorios::Recordatorio>();

    // Guarda parametros de recordatorio como arrays de caracteres en stack que existen por el scope de la func
    int id;
    char *fecha = new char[20];
    char *hora = new char[20];
    char *nota = new char [50];
    std::cout << "Ingrese ID de su recordatorio" << std::endl;
    std::cin >> id;
    std::cin.ignore(); // ignora newline que deja cin >>
    std::cout << "Ingrese la fecha en la que colocó recordatorio" << std::endl;
    std::cin.getline(fecha, 21, '\n');
    std::cout << "Ingrese la hora en la que colocó recordatorio" << std::endl;
    std::cin.getline(hora, 21, '\n');
    std::cout << "Ingrese el recordatorio" << std::endl;
    std::cin.getline(nota, 51, '\n');
    recordatorio->fecha = fecha;
    recordatorio->hora = hora;
    recordatorio->id = id;
    recordatorio->nota = nota;
    return recordatorio;


}

int main() {
    auto lista = std::make_shared<Recordatorios::Lista>();
    // Crear recordatorio
    bool salir = false;
    int opcion;
    int idSearch;
    while (!salir)
    {
        std::cout << "Eliga una opción" << '\n' << "1: Agregar recordatorio" << std::endl;
        std::cout << "2: Mostrar recordatorios actuales" << "\n3: Mostrar recordatorio por ID" << std::endl;
        std::cout << "3: Mostrar recordatorio por ID" << "\n4: Eliminar recordatorio por ID" << std::endl;
        std::cout << "Cualquier otro: Salir" << std::endl;
        std::cin >> opcion;
        std::cin.ignore();  // para ingorar el caracter \n que deja el cin >>
        switch (opcion)
        {
        case 1:
            lista->insertar_dato(CrearRecordatorio());
            break;
        
        case 2:
            lista->print_lista();
            break;
        case 3:
            std::cout << "Inserte ID de recordatorio a mostrar" << std::endl;
            std::cin >> idSearch;
            std::cin.ignore();
            lista->mostrar_dato(idSearch);
            break;
        case 4:
            std::cout << "Inserte ID de recordatorio a eliminar" << std::endl;
            std::cin >> idSearch;
            std::cin.ignore();
            lista->eliminar_dato(idSearch);
            break;
        default:
            salir = true;
            break;
        }
    }
    
return 0;
}
