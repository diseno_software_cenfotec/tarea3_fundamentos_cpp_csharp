#include <iostream>

#include "list.hpp"

using namespace Recordatorios;

void Lista::insertar_dato(std::shared_ptr<Recordatorio> dato) {
    std::shared_ptr<Node> nodo_nuevo = std::make_shared<Node>();
    nodo_nuevo->recordatorio = dato;   // agrega recordatorio al nuevo nodo

    if (this->head == nullptr) {   // no hay ningun dato en lista
      this->head = nodo_nuevo;
      this->tail = nodo_nuevo;
    }

    else {
      this->tail->next = nodo_nuevo;
      nodo_nuevo->prev = this->tail;
      this->tail = nodo_nuevo;
    }
    this->tamanho += 1;
}

void Lista::print_lista() {
  std::shared_ptr<Node> nodo_ref = this->head;
  while (nodo_ref != nullptr) {
    std::cout << nodo_ref->recordatorio->id << ", " << nodo_ref->recordatorio->fecha << ", " << nodo_ref->recordatorio->hora << '\n';
    std::cout << nodo_ref->recordatorio->nota << '\n' << std::endl;
    nodo_ref = nodo_ref->next;
  }
  std::cout << std::endl;
}

void Lista::mostrar_dato(int id)
{
  std::shared_ptr<Node> nodo_ref = this->head;
  if (nodo_ref != nullptr)
  {
    while (nodo_ref->recordatorio->id != id && nodo_ref->next != nullptr)
    {
        nodo_ref = nodo_ref->next;
    }
  }
  if (nodo_ref->recordatorio->id == id)
  {
    std::cout << nodo_ref->recordatorio->id << ", " << nodo_ref->recordatorio->fecha << ", " << nodo_ref->recordatorio->hora << '\n';
    std::cout << nodo_ref->recordatorio->nota << std::endl;
  }
  else
  {
    std::cout << "no se encontró dato";
  }
}

void Lista::eliminar_dato(int id){
  std::shared_ptr<Node> nodo_ref = this->head;
  std::shared_ptr<Node> nodo_prv = this->head;

  // caso: solo 1 dato en lista
  if (this->tamanho == 1) {
    nodo_ref.reset();
    this->head.reset();
    this->tail.reset();
    this->head = nullptr;
    this->tail = nullptr;
    return;
  }

  // busca id en la lista
  while (nodo_ref->recordatorio->id != id && nodo_ref->next != nullptr)
    {
        nodo_ref = nodo_ref->next;
    }

  // si el nodo a eliminar es el primero en lista
  if (nodo_ref == this->head) {
    this->head = this->head->next;
    this->head->prev.reset();
    this->head->prev = nullptr;
  }

  // si nodo a eliminar es último en lista 
  else if (nodo_ref == this->tail) {
    this->tail = nodo_prv;
    nodo_prv->next.reset();
    nodo_prv->next = nullptr;

  // nodo a eliminar está en el "medio"
  } else {
    nodo_prv->next = nodo_ref->next;
    nodo_prv->next->prev = nodo_prv;
  }
 
  nodo_ref->prev.reset();
  nodo_ref->prev = nullptr;
  nodo_ref.reset();
  this->tamanho -= 1;
}
